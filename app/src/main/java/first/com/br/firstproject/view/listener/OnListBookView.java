package first.com.br.firstproject.view.listener;

import android.view.View;

import java.util.List;

import first.com.br.firstproject.model.domain.Book;

/**
 * Created by enzo on 13/08/15.
 */
public interface OnListBookView {

    public void showMessage(String message);

    public void setItems(List<Book> books);

    public void onClickListener(View view, int position);


}
