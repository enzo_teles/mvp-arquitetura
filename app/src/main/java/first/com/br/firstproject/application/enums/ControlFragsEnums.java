package first.com.br.firstproject.application.enums;

import first.com.br.firstproject.view.fragment.AbstractFragment;
import first.com.br.firstproject.view.fragment.FirstFragment;
import first.com.br.firstproject.view.fragment.ListBookFragment;

/**
 * Here are all enums that will be used in the application, along with its keys and values
 * */
public enum ControlFragsEnums {

	MAIN	     			("main", FirstFragment.class),
	LISTBOOK				("list_book", ListBookFragment.class);
	
	private String name;
	private Class<? extends AbstractFragment> classFrag;
	
	private ControlFragsEnums(final String name, Class<? extends AbstractFragment> classFrag) {
		this.name = name;
		this.classFrag = classFrag;
	}
	
	public String getName() {
		return name;
	}
	public Class<? extends AbstractFragment> getClassFrag() {
		return classFrag;
	}
}
