package first.com.br.firstproject.application;

import java.sql.SQLException;

import android.app.Application;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import first.com.br.firstproject.model.domain.Book;
import first.com.br.firstproject.model.persistence.DataBaseHelper;

public class MVPApplication extends Application {

	private DataBaseHelper databaseHelper = null;

	private Dao<Book, Integer> bookDAO = null;

	@Override
	public void onCreate() {
		super.onCreate();
		databaseHelper = new DataBaseHelper(this);
	}

	public Dao<Book, Integer> getBookDao() throws SQLException {
		if (bookDAO == null) {
			bookDAO = databaseHelper.getDao(Book.class);
		}
		return bookDAO;
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		if (databaseHelper != null) {
			OpenHelperManager.releaseHelper();
			databaseHelper = null;
		}
	}

}
