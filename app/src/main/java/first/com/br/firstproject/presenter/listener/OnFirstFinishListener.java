package first.com.br.firstproject.presenter.listener;

public interface OnFirstFinishListener {

	void onUsernameError();

	void listBooks();
}
