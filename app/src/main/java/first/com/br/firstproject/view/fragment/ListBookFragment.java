package first.com.br.firstproject.view.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import br.org.mantra.android.leslie.Leslie;
import first.com.br.firstproject.R;
import first.com.br.firstproject.model.domain.Book;
import first.com.br.firstproject.presenter.business.ListBookPresenter;
import first.com.br.firstproject.presenter.listener.OnListBookPresenter;
import first.com.br.firstproject.view.adapter.BookAdapter;
import first.com.br.firstproject.view.listener.OnListBookView;

@SuppressLint("NewApi")
public class ListBookFragment extends AbstractFragment implements OnListBookView{
	//variable
	private RecyclerView listBook;
	private OnListBookPresenter presenter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = Leslie.with(getActivity()).bind(R.layout.list_book).to(this);

		listBook.setHasFixedSize(true);
		LinearLayoutManager llm = new LinearLayoutManager(getActivity());
		llm.setOrientation(LinearLayoutManager.VERTICAL);
		listBook.setLayoutManager(llm);

		listBook.addOnItemTouchListener(new ListBookPresenter(getActivity(), listBook, this));

		presenter = new ListBookPresenter(getActivity(), listBook, this);
		return view;
	}


	@Override
	public void onResume() {
		super.onResume();
		presenter.onResume();
	}

	/**
	 * method to show message
	 * */
	@Override
	public void showMessage(String message) {

		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}

	/**
	 * method to insert adapter in the lisView
	 * */
	@Override
	public void setItems(List<Book> books) {
		listBook.setAdapter(new BookAdapter(getActivity(), books));
	}

	/**
	 * OnClickListener adapter
	 * */
	@Override
	public void onClickListener(View view, int position) {
		Toast.makeText(getActivity(), "Pos: " + position, Toast.LENGTH_LONG).show();

	}
}

