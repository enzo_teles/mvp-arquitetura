package first.com.br.firstproject.presenter.business;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.util.List;

import first.com.br.firstproject.model.domain.Book;
import first.com.br.firstproject.presenter.actionInteractor.ListBookInteractor;
import first.com.br.firstproject.presenter.listener.OnListBookFinished;
import first.com.br.firstproject.presenter.listener.OnListBookInteractor;
import first.com.br.firstproject.presenter.listener.OnListBookPresenter;
import first.com.br.firstproject.view.listener.OnListBookView;

/**
 * Created by enzo on 13/08/15.
 */
public class ListBookPresenter implements OnListBookPresenter, OnListBookFinished, RecyclerView.OnItemTouchListener{
    //variable
    private OnListBookInteractor onListBookInteractor;
    private Context context;
    private GestureDetector gestureDetector;
    private OnListBookView mOnListBookView;


    public ListBookPresenter(Activity activity, final RecyclerView listBook, final OnListBookView mOnListBookView) {

        this.onListBookInteractor = new ListBookInteractor();
        this.context = context;
        this.mOnListBookView = mOnListBookView;

        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){

            @Override
            public boolean onSingleTapUp(MotionEvent e) {

                View cv = listBook.findChildViewUnder(e.getX(), e.getY());
                if(cv != null && mOnListBookView!= null){
                    mOnListBookView.onClickListener(cv, listBook.getChildAdapterPosition(cv));
                }
                return (true);
            }
        });

    }

    @Override
    public void onResume() {
        onListBookInteractor.findItems(this);
    }


     @Override
    public void showMessage() {
         mOnListBookView.showMessage("Show my listBook!!!");
    }

    @Override
    public void onFinished(List<Book> books) {
        if(books != null){
            mOnListBookView.setItems(books);
        }

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        gestureDetector.onTouchEvent(motionEvent);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean b) {

    }
}
