package first.com.br.firstproject.presenter.business;

import java.util.List;

import first.com.br.firstproject.R;
import first.com.br.firstproject.model.domain.Book;
import first.com.br.firstproject.model.facade.MvpBO;
import first.com.br.firstproject.application.enums.ControlFragsEnums;
import first.com.br.firstproject.presenter.listener.OnFirstFinishListener;
import first.com.br.firstproject.presenter.listener.OnFirstPresenter;
import first.com.br.firstproject.presenter.actionInteractor.FirstInteractor;
import first.com.br.firstproject.presenter.listener.OnFirstInteractor;
import first.com.br.firstproject.view.listener.OnFirstView;
import first.com.br.firstproject.view.listener.OnMainActivityView;

public class FirstPresenter implements OnFirstPresenter, OnFirstFinishListener {


	OnFirstView onFirstView;
	OnFirstInteractor onFirstInteractor;
	OnMainActivityView onMainActivityView;

	public FirstPresenter(OnFirstView onFirstView) {
		// TODO Auto-generated constructor stub
		this.onFirstView = onFirstView;
		this.onFirstInteractor = new FirstInteractor();
		onMainActivityView = (OnMainActivityView) MvpBO.getInstance().getContext();

	}

	@Override
	public void insertUser(String nome) {
		// TODO Auto-generated method stub
		onFirstInteractor.insert(nome, this);
		onFirstView.showMessage("nome "+ nome);
		
		List<Book> books = MvpBO.getInstance().getBooks();
        
        for (Book b : books) {
			onFirstView.showMessage("b " + b.getNome());
		}
		listBooks();
	}

	@Override
	public void onUsernameError() {
		// TODO Auto-generated method stub
		onFirstView.setUsernameError();
		
	}

	@Override
	public void listBooks() {

		onMainActivityView.transferFragment(ControlFragsEnums.LISTBOOK, R.id.layoutOne, true);

	}

}
