package first.com.br.firstproject.presenter.listener;

import java.util.List;

import first.com.br.firstproject.model.domain.Book;

/**
 * Created by enzo on 13/08/15.
 */
public interface OnListBookFinished {

    void onFinished(List<Book> books);
}
