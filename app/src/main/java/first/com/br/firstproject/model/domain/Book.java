package first.com.br.firstproject.model.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by enzo on 13/08/15.
 */
@DatabaseTable(tableName = "book")
public class Book implements Serializable{

    /**
     * Serial
     */
    private static final long serialVersionUID = 1L;
    @DatabaseField(generatedId=true, columnName="id")
    private int id;
    @DatabaseField(columnName = "name")
    private String nome;

    public Book() {
        // TODO Auto-generated constructor stub
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(final Object object) {
        final Book book;
        if(object instanceof Book){
            book = (Book) object;
            if(book.getId() == this.getId()){
                return true;
            } else {
                return false;
            }

        }
        return false;
    }

    @Override
    public int hashCode() {
        return getId();
    }


}