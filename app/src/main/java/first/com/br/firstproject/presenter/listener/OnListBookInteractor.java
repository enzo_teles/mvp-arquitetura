package first.com.br.firstproject.presenter.listener;

/**
 * Created by enzo on 13/08/15.
 */
public interface OnListBookInteractor {

    void findItems(OnListBookFinished listener);
}
