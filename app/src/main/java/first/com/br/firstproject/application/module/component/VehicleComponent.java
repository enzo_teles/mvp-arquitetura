package first.com.br.firstproject.application.module.component;

import javax.inject.Singleton;

import dagger.Component;
import first.com.br.firstproject.model.domain.Vehicle;
import first.com.br.firstproject.application.module.VehicleModule;

/**
 * Created by enzo on 15/08/15.
 */
@Singleton
@Component(modules = {VehicleModule.class})
public interface VehicleComponent {

    Vehicle provideVehicle();

}