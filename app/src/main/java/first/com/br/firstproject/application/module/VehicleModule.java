package first.com.br.firstproject.application.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import first.com.br.firstproject.model.domain.Motor;
import first.com.br.firstproject.model.domain.Vehicle;

/**
 * Created by enzo on 15/08/15.
 */
@Module
public class VehicleModule {

    @Provides @Singleton
    Motor provideMotor(){
        return new Motor();
    }

    @Provides @Singleton
    Vehicle provideVehicle(){
        return new Vehicle(new Motor());
    }
}
