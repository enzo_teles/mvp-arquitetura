package first.com.br.firstproject.model.facade;

import android.app.Activity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.Dao;

import first.com.br.firstproject.application.MVPApplication;
import first.com.br.firstproject.model.domain.Book;
import first.com.br.firstproject.application.utils.WrapperLog;



public class MvpBO {
	
	private static MvpBO instance;
	private static final Object SYNCOBJECT = new Object();
	private MVPApplication bookApplication;
	private Book book;
	public List<Book> books = new ArrayList<Book>();
	private Activity context;
	
	// variable persistence data
	private transient Dao<Book, Integer> bookDao;

	/**
	 * MVPBO is a Singleton
	 */
	public static MvpBO getInstance() {

		synchronized (SYNCOBJECT) {
			if (instance == null) {
				instance = new MvpBO();
			}
		}

		return instance;
	}

	// #############################################################
	// 							BOOK
	// #############################################################

	/**
	 * method to add a book in base
	 * 
	 * @param bookApp
	 * */
	public boolean insertBook(final Book book) {

		try {
			bookDao = getBookApplication().getBookDao();
			bookDao.create(book);
			WrapperLog.info("livro criado com sucesso!!!");
		} catch (SQLException e) {
			WrapperLog.info("error");
		}

		return true;
	}

	/**
	 * method to list a book
	 * */
	public List<Book> getListBook() {
		List<Book> books = null;

		try {
			bookDao = getBookApplication().getBookDao();
			books = bookDao.queryForAll();
			if (!books.isEmpty()) {
				return books;
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return books;

	}

	/**
	 * method to delete a book in base
	 * */
	public boolean deleteBook(final Book book) {

		try {
			bookDao.delete(book);
			return true;
		} catch (SQLException e) {
			WrapperLog.info("error");
			return false;
		}
	}

	/**
	 * method to update a book in base
	 * */
	public boolean updateBook(final Book book) {

		WrapperLog.info("book "+ book.getId());
		
		try {
			bookDao.createOrUpdate(book);
			WrapperLog.info("book: " + book.getNome());
			return true;
		} catch (SQLException e) {
			WrapperLog.info("error");
			return false;
		}
	}

	
	public MVPApplication getBookApplication() {
		return bookApplication;
	}

	public void setBookApplication(MVPApplication bookApplication) {
		this.bookApplication = bookApplication;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public List<Book> getBooks() {
		return books;
	}

	public Activity getContext() {
		return context;
	}

	public void setContext(Activity context) {
		this.context = context;
	}
}
