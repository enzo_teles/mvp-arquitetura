package first.com.br.firstproject.presenter.listener;

public interface OnFirstInteractor {
	
	void insert(String nome, OnFirstFinishListener listener);

}
