package first.com.br.firstproject.view.listener;


import first.com.br.firstproject.application.enums.ControlFragsEnums;

public interface OnMainActivityView {

   void registrerFragment(ControlFragsEnums frags, int layoutId , boolean addBackStack);
   void transferFragment(ControlFragsEnums frags, int layoutId , boolean addBackStack);

}
