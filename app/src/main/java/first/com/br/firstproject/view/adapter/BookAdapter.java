package first.com.br.firstproject.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import java.util.List;

import br.org.mantra.android.leslie.Leslie;
import first.com.br.firstproject.R;
import first.com.br.firstproject.model.domain.Book;

/**
 * Created by enzo on 18/08/15.
 */
public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder>{

    private List<Book> mList;
    private LayoutInflater layoutInflater;


    public BookAdapter(Context context, List<Book> mList){

        this.mList = mList;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public BookViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = layoutInflater.inflate(R.layout.item_book, viewGroup, false);
        BookViewHolder bvh = new BookViewHolder(v);

        return bvh;
    }

    @Override
    public void onBindViewHolder(BookViewHolder bookViewHolder, int i) {

        bookViewHolder.bookName.setText(mList.get(i).getNome());

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class BookViewHolder extends  RecyclerView.ViewHolder{

        public TextView bookName;

        public BookViewHolder(View itemView) {
            super(itemView);
            Leslie.with(itemView.getContext()).bind(itemView).to(this);
        }
    }
}
