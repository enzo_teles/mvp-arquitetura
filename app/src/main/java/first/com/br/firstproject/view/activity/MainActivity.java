package first.com.br.firstproject.view.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import first.com.br.firstproject.R;
import first.com.br.firstproject.application.MVPApplication;
import first.com.br.firstproject.model.domain.Vehicle;
import first.com.br.firstproject.model.facade.MvpBO;
import first.com.br.firstproject.application.enums.ControlFragsEnums;
import first.com.br.firstproject.view.listener.OnMainActivityView;


/**
 * Created by enzo on 13/08/15.
 */
@SuppressLint("NewApi")
public class MainActivity extends AbstractActivity implements OnMainActivityView {

    Vehicle vehicle;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);
        // inserting action bar
        toolbar = (Toolbar) findViewById(R.id.tb_main);
        toolbar.setTitle("Meu toobar");
        toolbar.setSubtitle("my subtitle");
        setSupportActionBar(toolbar);
        /**
         * method to add fragment in the backstack
         * */
        addFragment(ControlFragsEnums.MAIN, R.id.layoutOne, false);

        //application
        MvpBO.getInstance().setBookApplication((MVPApplication) getApplication());
        //context
        MvpBO.getInstance().setContext(this);

        //VehicleComponent component = Dagger_VehicleComponent.builder().vehicleModule(new VehicleModule()).build();

        //vehicle = component.provideVehicle();



    }

    /**
     * method to call the fragment
     * */
    @Override
    public void registrerFragment(ControlFragsEnums frags, int layoutId, boolean addBackStack) {
        registrerFragment(frags, layoutId, addBackStack);
    }
    /**
     * method to transfer the fragment
     * */

    @Override
    public void transferFragment(ControlFragsEnums frags, int layoutId, boolean addBackStack) {
        replaceFragment(frags,layoutId,addBackStack);
    }
}