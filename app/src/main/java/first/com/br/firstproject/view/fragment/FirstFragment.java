package first.com.br.firstproject.view.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import br.org.mantra.android.leslie.Leslie;
import first.com.br.firstproject.R;
import first.com.br.firstproject.presenter.business.FirstPresenter;
import first.com.br.firstproject.presenter.listener.OnFirstPresenter;
import first.com.br.firstproject.view.listener.OnFirstView;

@SuppressLint("NewApi")
public class FirstFragment extends AbstractFragment implements OnFirstView,
		OnClickListener {
	//variable
	private TextView bookNome;
	private TextView bookBook;
	private OnFirstPresenter presenter;



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = Leslie.with(getActivity()).bind(R.layout.frag).to(this);

		view.findViewById(R.id.button).setOnClickListener(this);
		// new presenter
		presenter = new FirstPresenter(this);

		return view;
	}

	/**
	 * method to show message in the screen
	 * */
	@Override
	public void showMessage(String message) {
		// TODO Auto-generated method stub
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}

	/**
	 * onClick
	 * */

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		presenter.insertUser(bookNome.getText().toString());
	}

	/**
	 * method to show error to user
	 * */
	@Override
	public void setUsernameError() {
		// TODO Auto-generated method stub
		bookNome.setError("endereco error");
	}


}
