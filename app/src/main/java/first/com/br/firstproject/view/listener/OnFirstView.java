package first.com.br.firstproject.view.listener;

public interface OnFirstView {
	
	public void setUsernameError();
	public void showMessage(String message);
}
