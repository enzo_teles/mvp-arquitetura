package first.com.br.firstproject.view.activity;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import first.com.br.firstproject.application.enums.ControlFragsEnums;
import first.com.br.firstproject.view.fragment.AbstractFragment;

@SuppressLint("NewApi")
public class AbstractActivity extends ActionBarActivity {

	protected FragmentManager fragmentManager;
	private AbstractFragment abstractFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		fragmentManager = getFragmentManager();
	}

	/**
	 * method to add fragment in backstack
	 * */
	public void addFragment(ControlFragsEnums control, int layoutId,
			boolean addBackStack) {

		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		try {
			abstractFragment = control.getClassFrag().newInstance();

			fragmentTransaction.add(layoutId, abstractFragment, control.getName());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("ERROR", e.getMessage());
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			Log.e("ERROR", e.getMessage());
		}

		if (addBackStack) {
			fragmentTransaction.addToBackStack(null);
		}
		fragmentTransaction.commit();

	}
	
	/**
	 * method to replace fragment in backstack
	 * */

	public void replaceFragment(ControlFragsEnums control, int layoutId,
			boolean addBackStack) {

		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		try {
			abstractFragment = control.getClassFrag().newInstance();

			fragmentTransaction.replace(layoutId, abstractFragment, control.getName());
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("ERROR", e.getMessage());
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			Log.e("ERROR", e.getMessage());
		}

		if (addBackStack) {
			fragmentTransaction.addToBackStack(null);
		}
		fragmentTransaction.commit();

	}
	
	/**
	 * method to remove fragment in backstack
	 * */

	public void removeFragment(ControlFragsEnums control, int layoutId) {

		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		fragmentTransaction.remove(abstractFragment);
		fragmentTransaction.commit();

	}
	
	
	
}
