package first.com.br.firstproject.presenter.actionInteractor;

import java.util.List;

import first.com.br.firstproject.model.domain.Book;
import first.com.br.firstproject.model.facade.MvpBO;
import first.com.br.firstproject.presenter.listener.OnListBookFinished;
import first.com.br.firstproject.presenter.listener.OnListBookInteractor;

/**
 * Created by enzo on 13/08/15.
 */
public class ListBookInteractor implements OnListBookInteractor {

    /**
     * method to get list of the data base
     * */
    @Override
    public void findItems(final OnListBookFinished listener) {


                List<Book> bookList = MvpBO.getInstance().getListBook();
                listener.onFinished(bookList);

    }
}
