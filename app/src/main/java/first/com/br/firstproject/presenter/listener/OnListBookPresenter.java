package first.com.br.firstproject.presenter.listener;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by enzo on 13/08/15.
 */
public interface OnListBookPresenter {

    void showMessage();
    void onResume();
}
