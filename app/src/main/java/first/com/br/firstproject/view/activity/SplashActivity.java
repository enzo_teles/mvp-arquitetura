package first.com.br.firstproject.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import first.com.br.firstproject.R;

public class SplashActivity extends AbstractActivity implements Runnable {

	private final int DELAY = 5000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		Handler handler = new Handler();
		handler.postDelayed(this, DELAY);
		
	}
	
	@Override
	public void run() {
		
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(intent);
		finish();	
	}

}
