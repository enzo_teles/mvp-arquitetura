package first.com.br.firstproject.presenter.actionInteractor;

import first.com.br.firstproject.model.domain.Book;
import first.com.br.firstproject.model.facade.MvpBO;
import first.com.br.firstproject.presenter.listener.OnFirstFinishListener;
import first.com.br.firstproject.presenter.listener.OnFirstInteractor;

public class FirstInteractor implements OnFirstInteractor {

    /**
     * method to insert book in the data base
     * */
	public void insert(final String nome, final OnFirstFinishListener listener){
		

                Book book = new Book();
                book.setNome(nome);
                MvpBO.getInstance().insertBook(book);



	}

}
